import './style.less';

import React from 'react';

const Header: React.FunctionComponent = () => <img id="logo" src="assets/wueww-logo-2022.svg" />;

export default Header;
